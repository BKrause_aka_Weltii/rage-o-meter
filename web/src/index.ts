const rageInputForm = document.getElementById(
  "rage-input-form"
) as HTMLFormElement;

const shareYouRageInput = document.getElementById(
  "share-your-rage"
) as HTMLInputElement;
const shareYouRageForm = document.getElementById(
  "share-your-rage-form"
) as HTMLFormElement;

const rageOMeter: HTMLElement = document.getElementById(
  "rage-o-meter"
) as HTMLElement;

let currentRage = 0;

function updateRageInputForm(currentRage): void {
  const input = rageInputForm.children[1] as HTMLInputElement;
  input.value = currentRage.toString();
}

function updateUrlOutputForm(newRage: number): void {
  const params: URLSearchParams = new URLSearchParams(window.location.search);

  params.set("current-rage", newRage.toString());

  const newURI = new URL(window.location.href);
  newURI.search = params.toString();

  (shareYouRageForm.children[1] as HTMLInputElement).value = newURI.toString();
}

function onFormSubmit(event?: Event): void {
  event?.preventDefault();

  const formData = new FormData(rageInputForm);
  const rage = formData.get("your-rage");
  if (rage) {
    currentRage = Number(rage);
    rageOMeter.setAttribute("current-rage", currentRage.toString());
    updateUrlOutputForm(currentRage);
  }
}

onFormSubmit();

rageInputForm.addEventListener("submit", onFormSubmit);
rageInputForm.addEventListener("input", onFormSubmit);
rageInputForm.addEventListener("change", onFormSubmit);

shareYouRageForm.addEventListener("submit", (event) => {
  event.preventDefault();

  const formData = new FormData(shareYouRageForm);
  const name = formData.get("your-name") ?? "a beautiful Person 😍";
  const rage = currentRage;
  const path = `${window.location.pathname}pages/others-rage?name=${name}&current-rage=${rage}`;
  const url = new URL(path, window.location.href + window.location.pathname).toString();

  if (navigator.share) {
    navigator.share({
      title: "Your current rage",
      url: url,
    });
  } else {
    navigator.clipboard.writeText(url);

    const button = shareYouRageForm.querySelector(
      "input[name='share-your-rage']"
    ) as HTMLInputElement | null;

    if (button) {
      button.value = "Done";

      setTimeout(() => {
        button.value =
          "Copy to Clipboard";
      }, 3000);
    }
  }
});
