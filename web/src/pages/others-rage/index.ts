function getParameterFromUrl(
  parameterName: string,
  defaultValue: string = ""
): string {
  let result: string | number | null = new URLSearchParams(
    window.location.search
  ).get(parameterName);

  if (result === null) {
    result = defaultValue;
  }

  return result;
}

let currentRage: number = Number(getParameterFromUrl("current-rage", "0"));

if (isNaN(currentRage) || currentRage < 0) {
  currentRage = 0;
} else if (currentRage > 1) {
  currentRage = 1;
}

const moodOfName = getParameterFromUrl("name", "a beautiful Person 😍");

document
  .getElementById("rage-o-meter")
  ?.setAttribute("current-rage", currentRage.toString());

const nameElements = document.querySelectorAll(".mood-of-name");

if (nameElements) {
  for (const element of Array.from(nameElements)) {
    if (element instanceof HTMLElement) {
      (element as HTMLElement).innerText = moodOfName;
    }
  }
}
document.title = `Rage of ${moodOfName}`;
