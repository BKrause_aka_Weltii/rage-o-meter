export interface PartDefinition {
  startAngle: number;
  endAngle: number;
  color: string;
  label: string;
}

const circleDegree = 180;

const minColor = 0;
const maxColor = 120;
const rangeColor = maxColor - minColor;

function getHslColor(totalCount: number, currentCount: number): string {
  const degree = maxColor - (rangeColor / totalCount) * currentCount;
  return `hsl(${degree}, 100%, 50%)`;
}

export const PART_DEFINITIONS: Array<PartDefinition> = [
  "Chilled",
  "Grumpy",
  "Angry",
  "Do not talk to me!",
].map((value: string, index: number, array: Array<string>) => {
  const color = getHslColor(array.length, index);
  const start = (circleDegree / array.length) * index;
  const end = start + circleDegree / array.length;
  return {
    color: color,
    endAngle: end,
    startAngle: start,
    label: value,
  };
});
