import { Vector2, Vector2Impl } from "./vector2";

export function getWindowSize(): Vector2 {
  return new Vector2Impl(window.innerWidth, window.innerHeight);
}

export const circleDegree = 180;

export const minColor = 0;
export const maxColor = 120;
export const rangeColor = maxColor - minColor;

export function getHslColor(totalCount: number, currentCount: number): string {
  const degree = maxColor - (rangeColor / totalCount) * currentCount;
  return `hsl(${degree}, 100%, 50%)`;
}
