import { PartDefinition } from "../parts";
import { circleDegree, getHslColor, getWindowSize } from "../utils";
import { Vector2, Vector2Impl } from "../vector2";

export class RageOMeter extends HTMLDivElement {
  private static namespace = "http://www.w3.org/2000/svg";
  private static defaultSections = [
    "Chilled",
    "Grumpy",
    "Angry",
    "Do not talk to me!",
  ];
  private static pointerImageUrl =
    "https://cdn.pixabay.com/photo/2020/08/24/08/50/hand-5513197_960_720.png";
  private static offset = -180;
  private static angToRad = Math.PI / 180;
  private static paddingInPercent = 10;
  private static relativePadding = 1 + this.paddingInPercent / 100;

  private rageSections: Array<string>;

  private rootContainer: SVGElement;
  private gRoot: SVGGElement;
  private pointer: SVGImageElement;

  private center: Vector2;
  private radius: number;
  private size: Vector2;

  private currentRageInPercent: number = 0;
  private parts: Array<PartDefinition>;

  constructor() {
    super();

    this.rootContainer = document.createElementNS(
      RageOMeter.namespace,
      "svg"
    ) as SVGElement;
    this.gRoot = document.createElementNS(
      RageOMeter.namespace,
      "g"
    ) as SVGGElement;
    this.pointer = RageOMeter.generatePointer();

    this.rootContainer.appendChild(this.gRoot);
    this.rootContainer.appendChild(this.pointer);

    this.appendChild(this.rootContainer);

    this.resize();

    if (this.hasAttribute("sections")) {
      this.rageSections = (this.getAttribute("sections") ?? "")
        .trim()
        .split(",")
        .map((part) => part.trim());
    } else {
      this.rageSections = RageOMeter.defaultSections;
    }

    this.parts = this.rageSections.map(
      (value: string, index: number, array: Array<string>) => {
        const color = getHslColor(array.length, index);
        const start = (circleDegree / array.length) * index;
        const end = start + circleDegree / array.length;
        return {
          color: color,
          endAngle: end,
          startAngle: start,
          label: value,
        };
      }
    );
    this.renderParts();
    this.updatePointer(this.currentRageInPercent);

    window.addEventListener("resize", this.resize.bind(this));
    window.addEventListener("resize", this.renderParts.bind(this));

    this.style.display = "inline-block";
    this.style.width = "fit-content";
    this.style.height = "fit-content";
  }

  static get observedAttributes() {
    return ["current-rage"];
  }

  public attributeChangedCallback(name, oldValue, newValue) {
    if (name === "current-rage") {
      this.updatePointer(newValue);
    }
  }

  private static generatePointer(): SVGImageElement {
    const pointer = document.createElementNS(
      RageOMeter.namespace,
      "image"
    ) as SVGImageElement;

    pointer.setAttribute("href", this.pointerImageUrl);
    pointer.setAttribute("width", "8%");

    return pointer;
  }

  private createPathElement(part: PartDefinition): SVGPathElement {
    const path = document.createElementNS(RageOMeter.namespace, "path");

    path.setAttribute("stroke", "none");
    path.setAttribute("fill", part.color);
    path.setAttribute(
      "d",
      this.calculatePath(
        this.center,
        this.radius,
        part.startAngle,
        part.endAngle
      )
    );

    return path;
  }

  private createTextElement(part: PartDefinition): SVGTextElement {
    const text = document.createElementNS(RageOMeter.namespace, "text");

    const tmpRadius = this.radius * 0.7;

    const positionStart = this.calculatePosition(
      this.center,
      tmpRadius,
      part.endAngle
    );
    const positionEnd = this.calculatePosition(
      this.center,
      tmpRadius,
      part.startAngle
    );

    const x = (positionEnd.x + positionStart.x) / 2;
    const y = (positionEnd.y + positionStart.y) / 2;

    text.setAttribute("x", x.toString());
    text.setAttribute("y", y.toString());
    text.setAttribute("fill", "black");

    text.innerHTML = part.label;

    return text;
  }

  private updatePointer(percent: number): void {
    const rotateParameters = [
      180 * percent + RageOMeter.offset / 2,
      this.center.x,
      this.center.y,
    ];

    this.pointer.setAttribute(
      "transform",
      `rotate(${rotateParameters.join(" ")})`
    );
  }

  private calculatePath(
    center: Vector2,
    radius: number,
    startAngle: number,
    endAngle: number
  ): string {
    const largeArcFlag = endAngle - startAngle <= 180 ? "0" : "1";

    const positionStart = this.calculatePosition(center, radius, endAngle);
    const positionEnd = this.calculatePosition(center, radius, startAngle);

    // help to draw arcs
    // https://svgwg.org/svg2-draft/paths.html#PathElement
    // https://stackoverflow.com/questions/5736398/how-to-calculate-the-svg-path-for-an-arc-of-a-circle
    // https://stackoverflow.com/questions/5300938/calculating-the-position-of-points-in-a-circle
    const parts = [
      "M",
      positionStart.x,
      positionStart.y,
      "A",
      radius,
      radius,
      0,
      largeArcFlag,
      0,
      positionEnd.x,
      positionEnd.y,
      "L",
      center.x,
      center.y,
    ];

    return parts.join(" ");
  }

  private calculatePosition(
    center: Vector2,
    radius: number,
    angle: number
  ): Vector2 {
    const angleInRadians = (angle - RageOMeter.offset) * RageOMeter.angToRad;

    return new Vector2Impl(
      radius * Math.cos(angleInRadians) + center.x,
      radius * Math.sin(angleInRadians) + center.y
    );
  }

  private resize() {
    this.size = getWindowSize();
    this.radius = this.size.smallerOne() / 2;
    this.center = new Vector2Impl(
      this.size.smallerOne() / 2,
      this.size.smallerOne() / 2
    );

    this.rootContainer.setAttribute("width", this.size.smallerOne().toString());
    this.rootContainer.setAttribute(
      "height",
      ((this.size.smallerOne() / 2) * RageOMeter.relativePadding).toString()
    );

    this.updatePointerPosition();
  }

  private renderParts() {
    this.gRoot.innerHTML = "";

    this.gRoot.append(
      ...this.parts.map((part) => this.createPathElement(part))
    );

    this.gRoot.append(
      ...this.parts.map((part) => this.createTextElement(part))
    );
  }

  private updatePointerPosition() {
    const boundingBox = this.pointer.getBBox();
    const width = boundingBox.width / 2;
    const height = boundingBox.height;
    this.pointer.setAttribute("x", (this.center.x - width).toString());
    this.pointer.setAttribute("y", (this.center.y - height).toString());
  }
}

customElements.define("rage-o-meter", RageOMeter, { extends: "div" });
