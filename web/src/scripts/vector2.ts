export interface Vector2 {
  x: number;
  y: number;

  smallerOne(): number;

  biggerOne(): number;
}

export class Vector2Impl implements Vector2 {
  constructor(private _x: number, private _y: number) {}

  smallerOne(): number {
    return Math.min(this._x, this._y);
  }

  biggerOne(): number {
    return Math.max(this._x, this._y);
  }

  get x(): number {
    return this._x;
  }
  get y(): number {
    return this._y;
  }
}
